<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\Like_dislike_question;
use Illuminate\Support\Facades\Auth;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answers = Answer::all();
        $questions = auth()->user()->questions;
        $title = 'Pertanyaan';
        return view('questions.index', compact('title', 'questions', 'answers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Tambah Pertanyaan';
        return view('questions.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);

        Auth()->user()->questions()->create([
            'title' => $question['title'],
            'body' => $question['body'],
        ]);

        return redirect(route('questions.index'))->with('success', 'Pertanyaan berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $likes = Like_dislike_question::where('question_id', $id)->get();
        $question = Question::find($id);
        $answers = Answer::where('question_id', $id)->get();
        $title = $question->title;
        // dd(auth()->user()->likes->where('question_id', $id));
        return view('questions.show', compact('question', 'title', 'answers', 'likes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        $title = 'Ubah Pertanyaan';
        return view('questions.edit', compact('question', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);

        Question::where('id', $id)->update([
            'title' => $question['title'],
            'body' => $question['body']
        ]);

        return redirect(route('questions.index'))->with('success-update', 'Perubahan berhasil disimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::where('id', $id)->delete();
        return redirect(route('questions.index'))->with('success-delete', 'Pertanyaan berhasil dihapus');
    }
}
