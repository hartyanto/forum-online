<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null(User::find($id)->profile_id)) {
            $profile = User::find($id);
            return view('users.edit', compact('profile'));
        } else {
            $profile = User::join('Profiles', 'users.profile_id', '=', 'profiles.id')
                ->where('users.id', $id)
                ->select('users.name', 'users.id', 'profiles.nama_lengkap', 'profiles.alamat', 'profiles.no_hp', 'profiles.photo')
                ->first();
            return view('users.edit', compact('profile'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = $request->validate([
            'nama_lengkap' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required|numeric'
        ]);

        if (is_null(User::find($id)->profile_id)) {
            Profile::Create([
                'nama_lengkap' => $profile['nama_lengkap'],
                'alamat' => $profile['alamat'],
                'no_hp' => $profile['no_hp'],
                'photo' => $request->photo
            ]);

            $profile2 = Profile::where('nama_lengkap', $profile['nama_lengkap'])->first();
            $profile_id = $profile2->id;
            User::where('id', $id)->update([
                'profile_id' => $profile_id
            ]);
        } else {
            $user_id = User::find($id)->profile_id;
            Profile::where('id', $user_id)->update([
                'nama_lengkap' => $profile['nama_lengkap'],
                'alamat' => $profile['alamat'],
                'no_hp' => $profile['no_hp'],
                'photo' => $request->photo
            ]);
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
