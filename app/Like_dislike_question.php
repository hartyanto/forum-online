<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like_dislike_question extends Model
{
    protected $fillable = ['question_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
