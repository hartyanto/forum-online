<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['nama_lengkap', 'alamat', 'no_hp', 'photo'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
