@extends('layouts.master')

@section('content')
    @forelse ($questions as $question)
    <div class="row mt-2">
        <div class="col">
            <h3><a class="card-link" href="{{ route('questions.show', $question->id) }}">{{ $question->title }}</a></h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-auto">
            {{ $loop->iteration }}
        </div>
        <div class="col text-right"">
            <p class="text-left">{{ $question->body }}</p>
            <small>
                ditanyakan pada {{ $question->created_at->format('M j, Y, g:i a') }}<br>
                <img style="width: 40px" src="{{ asset('img/no-photo.png') }}" alt="" class="rounded mr-2">oleh {{ $question->author->name }}
            </small>
            <div class="row">
                <div class="col">
                    <hr>
                </div>
            </div>
        </div>
    </div>
    @empty
    <div class="row mt">
        <div class="col">
            <h5 class="text-center">Belum ada pertanyaan</h5>
        </div>
    </div>
    @endforelse
    <div class="row">
        <div class="col">
            <div class="d-flex justify-content-center">
                {{ $questions->links()}}
            </div>
        </div>
    </div>
@endsection