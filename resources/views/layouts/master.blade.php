<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css')}}">
    <title>{{ $title ?? 'Forum Online' }}</title>
</head>
<body>
    @include('layouts.partials.navbar')
    <div class="container">
        @include('layouts.partials.alert')
        @yield('content')
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
</body>
</html>