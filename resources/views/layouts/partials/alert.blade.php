<div class="row">
    <div class="col mt-2">
        @if( session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif
        @if ( session('success-update'))
            <div class="alert alert-success">{{ session('success-update') }}</div>
        @endif
        @if ( session('success-delete'))
            <div class="alert alert-danger">{{ session('success-delete') }} </div>
        @endif
    </div>
</div>