@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col">
        <h3>Tambah pertanyaan</h3>
        <form action="{{ route('questions.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" value="{{ old('title') ?? '' }}" name="title" id="title" class="form-control">
                @error('title')
                        <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi pertanyaan</label>
                <textarea class="form-control" name="body" id="body" rows="10">{{ old('body') ?? '' }}</textarea>
                @error('body')
                        <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah Pertanyaan</button>
        </form>
    </div>
</div>
@endsection