@extends('layouts.master')

@section('content')
    <div class="row mt-2">
        <div class="col">
            <h3>Edit Pertanyaan</h3>
            <form action="{{ route('questions.update', $question->id ) }}" method="post">
                @csrf
                @method('PUT')                
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" value="{{ old('title') ?? $question->title }}" name="title" id="title" class="form-control">
                    @error('title')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Isi Pertanyaan</label>
                    <input type="text" value="{{ old('body') ?? $question->body }}" name="body" id="body" class="form-control">
                    @error('body')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Ubah</button>
            </form>
        </div>
    </div>
@endsection