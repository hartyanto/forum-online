@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col">
            <a href="{{ route('questions.create') }}" class="btn btn-primary">Tambah Pertanyaan</a>
            <table class="table mt-2">
                <thead class="thead-light">
                    <th scope="col">#</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Isi Pertanyaan</th>
                    <th scope="col">Jawaban</th>
                    <th scope="col" class="text-center">Action</th>
                </thead>
                <tbody>
                    @forelse ($questions as $question)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $question->title }}</td>
                            <td>{{ $question->body }}</td>
                            <td>{{ count( $answers->where('question_id', $question->id) ) }} jawaban</td>
                            <td class="d-flex justify-content-center">
                                <a href="{{ route('questions.show', $question->id) }}" class="btn btn-sm btn-primary">Show</a>
                                <a href="{{ route('questions.edit', $question->id) }}" class="btn btn-sm btn-success mx-2">Edit</a>
                                <form action="{{ route('questions.destroy', $question->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" class="text-center">TIDAK ADA DATA</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection