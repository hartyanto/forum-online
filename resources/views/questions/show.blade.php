@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col">
            <h3>{{ $question->title }}</h3>
            <small>Ditanyakan sejak {{ $question->created_at->diffForHumans() }}</small>
            <hr>
        </div>
    </div>
    <div class="row">
        {{------- Menampilkan likes untuk pertanyaan -------}}
        <div class="col-auto text-center">
            @if ( count($likes->where('user_id', Auth::id())) > 0 )
            <form action="{{ route('likes.destroy', $question->id) }}" method="post">
                @csrf
                @method('DELETE')
                <input type="hidden" name="question_id" value="{{ $question->id }}">
                <button type="submit" class="btn btn-primary btn-sm font-weight-bolder">^</button><br>
            </form>
            @else
            <form action="{{ route('likes.store') }}" method="post">
                @csrf
                <input type="hidden" name="question_id" value="{{ $question->id }}">
                <button type="submit" class="btn btn-secondary btn-sm font-weight-bolder">^</button><br>
            </form>
            @endif
            <h4 class="font-weight-bolder">{{ count($likes) }}</h4>
        </div>
        {{------- Akhir menampilkan likes untuk pertanyaan -------}}
        <div class="col text-right">
            <p class="text-left">{{ $question->body }}</p>
            <div class="btn text-right p-1" style="background-color: #e2ecf5">
                <small>Ditanyakan pada {{ $question->created_at->format('M j, Y, g:i a')}}</small><br>
                <img class="rounded mr-2" style="width: 40px;" src="{{ asset('img/no-photo.png') }}" alt=""><small>oleh {{ $question->author->name }}</small>
            </div>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col">
            Mempunyai {{ count($answers)}} jawaban
            <hr>
        </div>
    </div>
    @forelse ($answers as $answer)
    <div class="row">
        <div class="col-auto">#</div>
        <div class="col text-right">
            <p class="text-left">{{ $answer->body }}</p>
            <small class="text-right">Dijawab pada {{ $answer->created_at->format('M j, Y, g:i a')}}</small><br>
            <small>oleh {{ $answer->author->name }}</small>
            <hr>
        </div>
    </div>
    @empty
        <div class="row">
            <div class="col">
                <div class="text-center py-5 font-italic">Belum Ada Jawaban</div>
            </div>
        </div>
    @endforelse

    <div class="row">
        <div class="col">
            <form action="{{ route('answers.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <div class="bg-light my-2 py-2">
                        <label for="body" class="ml-4 font-weight-bolder">Jawabanmu</label>
                    </div>
                    <textarea class="form-control" name="body" id="body" rows="10" style="resize: none"></textarea>
                </div>
                <input type="hidden" name="question_id" value="{{ $question->id }}">

                @if (Auth::user())
                    <button type="submit" class="btn btn-primary">Tambah jawaban</button>
                @else
                    <a href="{{ route('login') }}" class="btn-primary btn">Login</a>
                @endif
            </form>
        </div>
    </div>
@endsection