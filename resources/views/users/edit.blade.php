@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col">
            <h3>Profile</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-auto">
        <img style="width: 200px;" src="{{ asset('img')}}/{{ $profile->photo ?? 'no-photo.png' }}" alt="">
        </div>
        <div class="col text-left">
            <h3 class="text-right">{{ $profile->username }}</h3>
            <form action="{{ route('users.update', $profile->id) }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="nama_lengkap">Nama Lengkap</label>
                    <input type="text" value="{{ $profile->nama_lengkap ?? '' }}" name="nama_lengkap" id="nama_lengkap" class="form-control">
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" value="{{ $profile->alamat ?? '' }}" name="alamat" id="alamat" class="form-control">
                </div>        
                <div class="form-group">
                    <label for="no_hp">No HP</label>
                    <input type="text" value="{{ $profile->no_hp ?? '' }}" name="no_hp" id="no_hp" class="form-control">
                </div>    
                <div class="form-group">
                    <label for="photo">Photo</label>
                    <input type="text"  value="{{ $profile->photo ?? '' }}" name="photo" id="photo" class="form-control">
                </div>    

                <button type="submit" class="btn btn-primary">Ubah</button>
            </form>
        </div>
    </div>
@endsection