<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Route::get('/questions/create', 'QuestionsController@create')->middleware('auth')->name('questions.create');
Route::middleware('auth')->group(function () {
    Route::get('/questions', 'QuestionsController@index')->name('questions.index');
    route::post('/questions', 'QuestionsController@store')->name('questions.store');
    route::get('/questions/{question_id}/edit', 'QuestionsController@edit')->name('questions.edit');
    route::put('/questions/{question_id}', 'QuestionsController@update')->name('questions.update');
    route::delete('/questions/{question_id}', 'QuestionsController@destroy')->name('questions.destroy');
});
route::get('/questions/{question_id}/show', 'QuestionsController@show')->name('questions.show');

Route::post('/answers', 'AnswersController@store')->name('answers.store');

Route::post('/likes', 'LikesController@store')->name('likes.store');
Route::delete('/likes/{question_id}', 'LikesController@destroy')->name('likes.destroy');

Route::get('/users/{user_id}/edit', 'UsersController@edit')->name('users.edit');
Route::post('/users/{user_id}', 'UsersController@update')->name('users.update');

Auth::routes();

Route::get('users');

// Route::get('/home', 'HomeController@index')->name('home');
